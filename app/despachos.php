<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class despachos extends Model
{
    use Notifiable;
    public $timestamps = 'despachos';

    protected $fillable = ['id','marca', 'modelo', 'color', 'Placa','ciudad'];
}
