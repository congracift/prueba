<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Prueba</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery.js') }}"></script>

        
      
    </head>
    <body>
        <main class="py-4">
           <table class="table table-striped" id="tabla">
            <thead>
                <th style="width: 15px;"><input id="selectall" type="checkbox" name="Marcar todos"></th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Color</th>
                <th>Placa</th>
                <th>Ciudad Destino</th>
            </thead>
            <tbody>
                @foreach($despachos as $despacho)
                <tr class="fila">
                    <td><input value={{$despacho->id}} class="selectall" type="checkbox"></td>
                    <td>{{ $despacho->marca }}</td>
                    <td>{{ $despacho->modelo }}</td>
                    <td>{{ $despacho->color }}</td>
                    <td>{{ $despacho->Placa }}</td>
                    <td name="ciudad">{{ $despacho->ciudad }}</td>
                </tr>
                @endforeach
            </tbody>
            </table> 
        </main>
    </body>
    <footer>
        <script src="{{ asset('js/app.js') }}"></script>
        
        <script>
      $(document).ready(function () {  
        //Detectar click en el checkbox superior de la lista
        $('#selectall').on('click', function () {
          //verificar el estado de ese checkbox si esta marcado o no
          var checked_status = this.checked;
 
          /*
           * asignarle ese estatus a cada uno de los checkbox
           * que tengan la clase "selectall"
          */
          $(".selectall").each(function () {
            this.checked = checked_status;
          });
        });
      });

      $(document).ready(function(){
          $(".selectall").each(function ()
          {
              if($(this).is(':checked'))
              {
                  ciudades = [];
                  $("#tabla tr").find('td:eq(5)').each(function () {
 
                    //obtenemos el valor de una fila con respecto a la ciudad
                    ciudad = $(this).html();
 
                    //agregamos dentro de un array todos los datos
                    ciudades.push(ciudad);
                    });
                    
                    alert(ciudades[0]);
              }
          });
      });
        //Esta funcion sirve para contar la cantidad de veces que esta una ciudad repetida
        function ContarRepetidos(ciudades) {
            contador = 0;
            ciudad = "";
            var autosCiudad = new Array(2);
            autosCiudad[0] = new Array(2);
            autosCiudad[1] = new Array(2);
            for (i=0; i < ciudades.length; i++) 
            {
                for(j=0; j < ciudades.length; j++)
                {

                    if(i == 0)
                    {
                        if(ciudades[i] == ciudades[j] )
                        {
                            contador++;
                        }
                    }else
                    {
                        if(ciudades[i] == ciudades[j] && !yaVerificado(ciudades[j],autosCiudad))
                        {
                            contador++;
                        }
                    }
                }
                if(contador != 0)
                {
                    autosCiudad[i][0] = ciudades[i];
                    autosCiudad[i][1] = contador;
                }
                contador=0;
                

            }

            return autosCiudad;
            
        }
        //Validamos si la la ciudad que pasa como parametro
        //ya fue contada o no
        function yaVerificado(ciudad,autosCiudad)
        {
            for(i=0;i<autosCiudad.length;i++){
                if(autosCiudad[i] == ciudad)
                {
                    return true;
                }
            }
            return false;
        }
    </script>
    </footer>
</html>
